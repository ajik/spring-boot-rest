package id.webapp.app.controller;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import id.webapp.app.model.Personal;
import id.webapp.app.model.Response;
import id.webapp.app.service.PersonalService;

@RestController
public class HomeController {
	
	@Autowired
	private PersonalService personalService;

	@GetMapping(value="/method/post")
	public Response post(@RequestParam String name, @RequestParam String city) {
		
		// System.out.println(name + " &&& " + city);
		
		Personal personal = new Personal();
		personal.setName(name);
		personal.setCity(city);
		Response res = new Response();
		
		try {
			String insertToDb = personalService.postPersonal(personal);
			
			
			if(insertToDb == "SUCCESS") {
				res.setCode(200);
				res.setStatus(insertToDb);
				res.setMessage("");
			} else {
				res.setCode(400);
				res.setStatus("");
				res.setMessage(insertToDb);
			}
		} catch(Exception e) {
			res.setCode(500);
			res.setStatus("");
			res.setMessage("Error: " + e.toString());
		}
		
		return res;
	}
	
	@GetMapping(value="/method/put")
	public Response put(@RequestParam String new_name, @RequestParam String new_city, @RequestParam Long id) {
		Response res = new Response();
		String newName = new_name;
		String newCity = new_city;
		
		try {
			Personal personal = new Personal();
			personal.setId(id);
			personal.setCity(newCity);
			personal.setName(newName);
			String status = personalService.putPersonal(personal);
			if(status == "SUCCESS") {
				res.setCode(200);
				res.setStatus(status);
				
			} else {
				res.setCode(400);
				res.setMessage(status);
			}
			
		} catch(Exception e) {
			res.setCode(500);
			res.setMessage("Error: " + e.toString());
		}
		
		return res;
	}
	
	@GetMapping(value="/method/delete")
	public Response delete(@RequestParam Long id) {
		Response res = new Response();
		
		try {
			String status = personalService.deletePersonal(id);
			
			if(status == "SUCCESS") {
				res.setCode(200);
				res.setStatus(status);
			} else {
				res.setCode(400);
				res.setMessage(status);
			}
		}catch (Exception e) {
			// TODO: handle exception
			res.setCode(500);
			res.setMessage("Error: " + e.toString());
		}
		return res;
	}
	
	@GetMapping(value="/method/get")
	public ResponseEntity<List<Personal>> get() {
		List<Personal> personal = personalService.getAllPersonal();
		return new ResponseEntity<List<Personal>>(personal, HttpStatus.OK);
	}
	
	@GetMapping(value="/method/get2")
	public Response get2() {
		Response res = new Response();
		
		try {
			List<Personal> personal = personalService.getAllPersonal();
			res.setData(personal);
			res.setCode(200);
			res.setStatus("success");
		} catch (Exception e) {
			// TODO: handle exception
			res.setCode(500);
			res.setMessage("Error: " + e.toString());
		}
		return res;
	}
}
