package id.webapp.app.dao.impl;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.stereotype.Repository;

import id.webapp.app.dao.PersonalDao;
import id.webapp.app.model.Personal;
 
@Repository
public class PersonalDaoImpl implements PersonalDao{
	
	@PersistenceContext
	private EntityManager em;
	
	@Override
	public Personal getPersonalById(Long id) {
		// TODO Auto-generated method stub
		return em.find(Personal.class, id);
	}

	@Override
	public String postPersonal(Personal personal) {
		// TODO Auto-generated method stub
		try {
			em.persist(personal);
			return "SUCCESS";
		} catch (Exception e) {
			// TODO: handle exception
			return "ERROR: " + e;
		}
		
	}
	
	@Override
	public String putPersonal(Personal personal) {
		// TODO Auto-generated method stub
		
		try {
			em.merge(personal);
			return "SUCCESS";
		} catch(Exception e) {
			return "ERROR: " + e;
		}
	}

	
	@Override
	public String deletePersonal(Long id) {
		// TODO Auto-generated method stub
		try {
			em.remove(getPersonalById(id));
			return "SUCCESS";
		} catch (Exception e) {
			// TODO: handle exception
			return "ERROR: " + e.toString();
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Personal> getAllPersonal() {
		// TODO Auto-generated method stub
		String hql = "from Personal";
		return (List<Personal>) em.createQuery(hql).getResultList();
	}
	
}
