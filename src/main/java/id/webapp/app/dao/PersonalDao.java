package id.webapp.app.dao;

import java.util.List;

import id.webapp.app.model.Personal;

public interface PersonalDao {

	public String postPersonal(Personal personal);
	public String putPersonal(Personal personal);
	public String deletePersonal(Long id);
	
	public Personal getPersonalById(Long id);
	
	public List<Personal> getAllPersonal();
}
