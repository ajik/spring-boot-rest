package id.webapp.app.service.impl;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import id.webapp.app.dao.PersonalDao;
import id.webapp.app.model.Personal;
import id.webapp.app.service.PersonalService;

@Service
@Transactional
public class PersonalServiceImpl implements PersonalService {

	@Autowired
	private PersonalDao personalDao;
	
	@Override
	public String postPersonal(Personal personal) {
		// TODO Auto-generated method stub
		return personalDao.postPersonal(personal);
	}

	@Override
	public String putPersonal(Personal personal) {
		// TODO Auto-generated method stub
		return personalDao.putPersonal(personal);
	}

	@Override
	public String deletePersonal(Long id) {
		// TODO Auto-generated method stub
		return personalDao.deletePersonal(id);
	}

	@Override
	public List<Personal> getAllPersonal() {
		// TODO Auto-generated method stub
		return personalDao.getAllPersonal();
	}

}
