package id.webapp.app.service;

import java.util.List;

import id.webapp.app.model.Personal;

public interface PersonalService {
	
	public String postPersonal(Personal personal);
	public String putPersonal(Personal personal);
	// public Personal getPersonalById(Long id);
	public String deletePersonal(Long id);
	public List<Personal> getAllPersonal();

}
