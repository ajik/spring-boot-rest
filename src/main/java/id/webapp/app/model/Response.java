package id.webapp.app.model;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class Response {

	private Integer code;
	private String status;
	private String message;
	private List<Personal> data;
	
	public Integer getCode() {
		return code;
	}
	public void setCode(Integer code) {
		this.code = code;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public List<Personal> getData() {
		return data;
	}
	public void setData(List<Personal> data) {
		this.data = data;
	}
	
}
